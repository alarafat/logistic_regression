function plotData(X, y)
%PLOTDATA Plots the data points X and y into a new figure 
%   PLOTDATA(x,y) plots the data points with + for the positive examples
%   and o for the negative examples. X is assumed to be a Mx2 matrix.

% Create New Figure
figure; hold on;

% ====================== YOUR CODE HERE ======================
% Instructions: Plot the positive and negative examples on a
%               2D plot, using the option 'k+' for the positive
%               examples and 'ko' for the negative examples.
%
inverted_y = 1.-y;
positive_samples = X.*y;
negative_samples = X.*inverted_y;
positive_samples = positive_samples(any(positive_samples, 2), :);
negative_samples = negative_samples(any(negative_samples, 2), :);

scatter(positive_samples(:, 1), positive_samples(:, 2), 'k+')
scatter(negative_samples(:, 1), negative_samples(:, 2), 'ko')

% =========================================================================

hold off;

end
