**Logistic Regression in Matlab & Python**

This implementation contains solution to the exercise 2 of the Coursera Machine Learning course by Andrew Ng.
Exercise 2 is to implement Logistic Regression using given data. 
The original assignment requires Matlab solution. The additional Python implementation is just mirroring the Matlab implementation.

The regularized cost function used is :
![alt text](images/cost.png?raw=true "cost function")

The equations to compute gradient are:
![alt text](images/gradient.png?raw=true "gradient")

The result using 'ex2data1.txt' for linear classification is:
![alt text](images/Figure_1.png?raw=true "linear_class")

The result using 'ex2data2.txt' for non-linear classification is:
![alt text](images/Figure_2.png?raw=true "non_linear_class")

**To run the code in Matlab**
- Run the *ex2.m* for linear classification and *ex2_reg.m* for non-linear classification.

**To run the code in Python**
- Run the *logistic_regression.py* and change the options at the beginning of the code.
- set the configurations, as *decision_boundary_type* and *file_name* to use either linear or non-linear classification.
*ex2data1.txt* contains data that can be used to test linear classification while *ex2data2.txt* contains data to be used to test non-linear classification.
  