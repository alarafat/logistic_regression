import os
import enum
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import fmin_bfgs


# directory of this file
this_dir = os.path.dirname(os.path.realpath(__file__))


class DecisionBoundaryType(enum.Enum):
    """
    decision boundary types
    """
    linear = 'linear'
    non_linear = 'non_linear'


# Configuration of the logistic regression model
file_name = 'ex2data2.txt'                  # file name to load the data from
decision_boundary_type = DecisionBoundaryType.non_linear        # decision boundary type to classify data, either linear or non-linear
# hyper parameters for the logistic regression model
HYPER_PARAMETER = dict(use_regularizer=True,
                       lambda_=10,
                       maxiter=400)


def logistic_regression(theta, x, y):
    """

    Parameters
    ----------
    theta   :   numpy.ndarray
       parameters
    x   :   numpy.ndarray
       input data
    y   :   numpy.ndarray
        label

    """
    use_regularizer = HYPER_PARAMETER.get('use_regularizer')
    lambda_ = HYPER_PARAMETER.get('lambda_')
    max_iter = HYPER_PARAMETER.get('maxiter')

    j = compute_cost(theta, x, y, use_regularizer=use_regularizer, lambda_=lambda_)
    grad = compute_gradient(theta, x, y, use_regularizer=use_regularizer, lambda_=lambda_)
    print('for given theta, the cost, J and computed gradient are: {} and {}'.format(j, grad))

    # bfgs optimization techniques has been used to get optimal theta value.
    theta_opt = fmin_bfgs(compute_cost, x0=theta, fprime=compute_gradient, maxiter=max_iter, args=(x, y))
    print('Optimal parameter values are {}'.format(theta_opt))
    plot_decision_boundary(x, theta_opt)


def feature_map(x1, x2, degree):
    """
    create non-linear feature map
    --> [1  x1  x2  x1^2    x1*x2   x2^2    x1^3    x1^2*x2     x1*x2^2 .....      .... x1*x5^5   x2^6]
    Parameters
    ----------
    x1  :   np.ndarray
    x2  :   np.ndarray
    degree  :   int
        polynomial degree

    Returns
    -------
    feature map
    """
    x1 = np.atleast_1d(x1)
    x2 = np.atleast_1d(x2)
    output_matrix = np.ones((len(x1), 1))
    for i in range(1, degree + 1):
        for j in range(i + 1):
            output_matrix = np.column_stack((output_matrix, (x1**(i - j) * x2**j)))
    return output_matrix


def compute_cost(theta, x, y, use_regularizer=False, lambda_=None):
    """
        computed J (Equation used can be found in the README)
        Parameters
        ----------
        theta   :   numpy.ndarray
            parameters
        x   :   numpy.ndarray
           input data
        y   :   numpy.ndarray
            label
        use_regularizer :   bool
            whether to use regularization
        lambda_ :   float
            hyper-parameter to be used to control regularization

        Returns
        -------
        j    :   float
            cost value

        """
    theta = np.atleast_2d(theta)
    number_samples = len(x)
    number_parameters = theta.shape[-1]
    hypothesis = sigmoid(np.dot(theta, x.T))

    regularizer = 0.0
    if use_regularizer:
        if lambda_ is None:
            raise RuntimeError('lambda cannot be none.')
        regularizer = (lambda_ / (2 * number_samples)) * np.sum(theta[:, 1:number_parameters] ** 2)
    j = (1 / number_samples) * (np.sum(-(y * np.log(hypothesis)) - ((1 - y) * np.log(1 - hypothesis)))) + regularizer
    return j


def compute_gradient(theta, x, y, use_regularizer=False, lambda_=None):
    """
    computed gradients (Equation used can be found in README)
    Parameters
    ----------
    theta   :   numpy.ndarray
        parameters
    x   :   numpy.ndarray
       input data
    y   :   numpy.ndarray
        label
    use_regularizer :   bool
        whether to use regularization
    lambda_ :   float
        hyper-parameter to be used to control regularization

    Returns
    -------
    grad    :   numpy.ndarray
        gradient values

    """
    theta = np.atleast_2d(theta)
    number_samples = len(x)
    number_parameters = theta.shape[-1]
    hypothesis = sigmoid(np.dot(theta, x.T))

    grad_regularizer = 0.0
    if use_regularizer:
        if lambda_ is None:
            raise RuntimeError('lambda cannot be none')
        grad_regularizer = np.zeros(theta.shape)
        grad_regularizer[:, 1:number_parameters] = (lambda_ / number_samples) * theta[:, 1:number_parameters]
    grad = (1 / number_samples) * np.dot((hypothesis - y), x) + grad_regularizer
    return np.squeeze(grad)


def sigmoid(z):
    return 1 / (1 + np.exp(-z))


def plot_data(x, y):
    """
    this method plots the data as positive and negative samples
    Parameters
    ----------
    x   :   numpy.ndarray
        input data
    y   :   numpy.ndarray
        label

    """
    positive_points = x[y > 0]
    negative_points = x[y == 0]
    plt.scatter(positive_points[:, 0], positive_points[:, 1], marker='+', c='k')
    plt.scatter(negative_points[:, 0], negative_points[:, 1], marker='o', c='k')
    plt.gca().legend(('Admitted', 'Not admitted'), loc=1)


def plot_decision_boundary(x, theta):
    """
    This method plot decision bounday. The decision boundary can be linear one (line) or contour
    Parameters
    ----------
    x   :   numpy.ndarray
        input data
    theta   :   numpy.ndarray
        parameters

    """
    if x.shape[-1] <= 3:
        plot_x = np.array([np.min(x[:, 1]) - 2, np.max(x[:, 2])])
        plot_y = (-1/theta[2]) * (theta[1] * plot_x + theta[0])
        plt.plot(plot_x, plot_y)
        plt.gca().legend(('Decision Boundary', 'Admitted', 'Not admitted'), loc=0)
    else:
        u = np.linspace(-1, 1.5, 50)
        v = np.linspace(-1, 1.5, 50)
        z = np.zeros((len(u), len(v)))
        for i in range(len(u)):
            for j in range(len(v)):
                z[i, j] = np.dot(feature_map(u[i], v[j], 6), theta)
        z = z.T
        plt.contour(u, v, z, 0)
        plt.gca().legend(('Admitted', 'Not admitted'), loc=0)
    plt.show(block=True)


def load_data():
    """
    Loading data from given text file

    """
    train_data = np.loadtxt(os.path.join(this_dir, '../data', file_name), delimiter=',', dtype=str).astype(np.float)
    data = train_data[:, 0:2]
    label = train_data[:, -1]
    return data, label


def main():
    # First, load data and label form the given text file
    data, label = load_data()
    plot_data(data, label)
    data = np.column_stack((np.ones((data.shape[0])), data))

    if decision_boundary_type == DecisionBoundaryType.non_linear:
        data = feature_map(data[:, 1], data[:, 2], degree=6)
        theta = np.ones((1, data.shape[-1]))
    else:
        # theta = np.zeros((1, data.shape[-1]))
        theta = np.array([-24, 0.2, 0.2])

    # calling logistic regression model
    logistic_regression(theta, data, label)


if __name__ == '__main__':
    main()
